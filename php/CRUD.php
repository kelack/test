<?php
    class DataBase {
        private $arrayJson; 
        public function __construct()
        {   
            // //Создание из файла 'bd.json' массивa
            $this->arrayJson = json_decode(file_get_contents('../json/bd.json'),true);
        }

        public function Create($userValues)
        {
            //создание массива из значений введенных пользователем
            foreach($userValues as $key => $value)
            {
                //проверка login на сопадение (проход по всему $arrayJson)
                if($key=='login')
                    foreach($this->arrayJson as $numberUser => $item)
                        foreach($item as $fieldNames => $fieldValue)
                            if($fieldNames=='login' and $fieldValue == $value)
                                return 'Такой login уже есть';

                //проверка email на сопадение (проход по всему $arrayJson)
                if($key=='email')
                    foreach($this->arrayJson as $numberUser => $item)
                        foreach($item as $fieldNames => $fieldValue)
                            if($fieldNames=='email' and $fieldValue == $value)
                                return 'Такой email уже есть';

                //сохранене 'password' для дальнейшей проверки
                if($key=='password')
                    $password = $value;

                //проверка паролей на совпадение
                if($key=='confirm_password' and $value!==$password)
                    return 'Пароли не совпадают';
        
                //чтобы не добавлять 'confirm_password' в массив
                if($key!=='confirm_password')
                    if($key!=='password')
                        //добавление значений в массив
                        $userDataArray[count($this->arrayJson)+1][$key] = $value;
                    else
                        //Добавление измененного пароля
                        $userDataArray[count($this->arrayJson)+1][$key] = "соль".$value;
            }
            
            //Добавление в старый массив, новый массив введенный пользователем
            $this->arrayJson += $userDataArray;   

            //Преобразование массива в json
            $jsonData = json_encode($this->arrayJson);

            //Добавление json в файл 'bd.json'
            file_put_contents('../json/bd.json',$jsonData);

            return $userValues["name"];
        }

        public function Read($userValues)
        {
            $id = '';
            //Проходимся по arrayJson и ищим совпадение на 'login' введеный пользователем
            foreach($this->arrayJson as $numberUser => $item)
                foreach($item as $fieldNames => $fieldValue)
                    if($fieldNames == 'login' and $userValues["login"] == $fieldValue)
                    {
                        //Как только нашли такой же 'login', сохраняем id массива и останавливаем циклы
                        $id = $numberUser;
                        break 2;
                    }
            
            //Если мы не нашли 'login', выводим сообщение
            if($id == '')
                return 'login введен не правильно';
            
            //Если 'login' был найден, ищем "password"
            if($this->arrayJson[$id]['password'] == ("соль".$userValues["password"]))
                return $this->arrayJson[$id]['name'];

            //Если мы не нашли 'password', выводим сообщение
            else
                return 'password введен не правильно'; 
        }
    }
?>