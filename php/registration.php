<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
// Если к нам идёт Ajax запрос
    session_start();
    $_SESSION['error'] = '';
    $_SESSION['name'] = '';
    $_COOKIE["name"] = '';


    //Проверка на заполненность полей
    $errors = [];
    foreach($_POST as $key => $value)
    {
        //Добавление ошибок
        if(empty($value))
            $errors[] = "$key не заполнин; ";
    }

    //Если есть ошибки, их вывод
    if(!empty($errors))
    {
        foreach($errors as $err)
        $_SESSION['error'] .= $err;
    }
    //Если поля заполнены, добавление в bd.json
    else
    {
        require 'CRUD.php';

        $user = new DataBase();

        //Добавляем данные с функции в переменную, для дальнейшей проверки
        $check = $user->Create($_POST);
        echo $check;
        //если переменная строка, добавляем ее в 'error', в противном случае добавляем ее в 'id'
        if($check == 'Такой login уже есть' or $check == 'Такой email уже есть'  or $check == 'Пароли не совпадают')
            $_SESSION['error'] = $check;
        else
        {
            $_SESSION['name'] = $check; 
            $_COOKIE["name"] = $check; 
        }
            
    }
}
//Если это не ajax запрос
exit;
?>