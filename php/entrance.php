<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
// Если к нам идёт Ajax запрос
    session_start();
    $_SESSION['error'] = '';
    $_SESSION['name'] = '';
    $_COOKIE["name"] = '';

    //Проверка на заполненность полей
    $errors = [];
    foreach($_POST as $key => $value)
    {
        //Добавление ошибок
        if(empty($value))
            $errors[] = "$key не заполнин; ";
    }

    //Если есть ошибки, их вывод
    if(!empty($errors))
    {
        foreach($errors as $err)
            echo $err;
    }
    //Если ошибок нет, добавление в bd.json

        require 'CRUD.php';

        $user = new DataBase();

        //Добавляем данные с функции в переменную, для дальнейшей проверки
        $check = $user->Read($_POST);
        //если переменная строка, добавляем ее в 'error', в противном случае добавляем ее в 'id'
        if($check == 'login введен не правильно' or $check == 'password введен не правильно')
            $_SESSION['error'] = $check;
        else
        {
            $_SESSION['name'] = $check; 
            $_COOKIE["name"] = $check; 
        } 
}
//Если это не ajax запрос
exit;
?>